### Para poder ejecutar la aplicación Web en Angular por favor haga lo siguiente: ###

1. Clone el repositorio en una carpeta vacía dentro de su disco local.
2. Verifique que tenga Angular instalado o ejecute el siguiente comando "npm install -g @angular/cli"
3. Asegurese de tener la API REST en ejecución. 
4. Abra la consola de comandos, ubiquese en la carpeta "prypersonas" y ejecute la aplicación web con el comando "ng serve". 
5. Cuando se haya ejecutado la aplicación le debe aparecer un enlace de tipo http://localhost:4200/  copielo y pongalo en el navegador para acceder a la misma.