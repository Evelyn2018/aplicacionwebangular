import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Persona } from '../models/persona.model';

const baseUrl = 'http://localhost:4000/personas';


@Injectable({
  providedIn: 'root'
})
export class SrvPersonasService {

  constructor(private http: HttpClient) { }

  obtenerTodos(): Observable <Persona[]>{
    return this.http.get<Persona[]>(baseUrl);
  }

  obtener(id: any): Observable <Persona>{
    return this.http.get(baseUrl+"/"+id);
  }

  agregar(datos: any): Observable <any>{
    return this.http.post(baseUrl, datos);
  }
  actualizar(id: any, datos: any): Observable<any>{
    return this.http.put(baseUrl+"/"+id, datos);
  }
}
