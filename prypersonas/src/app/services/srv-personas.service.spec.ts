import { TestBed } from '@angular/core/testing';

import { SrvPersonasService } from './srv-personas.service';

describe('SrvPersonasService', () => {
  let service: SrvPersonasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SrvPersonasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
