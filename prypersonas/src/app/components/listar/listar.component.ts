import { Component, OnInit } from '@angular/core';
import { Persona } from 'src/app/models/persona.model';
import { SrvPersonasService } from 'src/app/services/srv-personas.service';


@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {

  lstPersonas?: Persona[ ];
  seleccionada : Persona = {id: 0};
  indice = -1;

  constructor(private servicio: SrvPersonasService) { }

  ngOnInit(): void {
    this.obtenerPersonas();
  }

  obtenerPersonas(): void {
    console.log("obtendra las personas");
    this.servicio.obtenerTodos()
        .subscribe(
          respuesta => {
            this.lstPersonas =respuesta;
            console.log(this.lstPersonas);
          }, 
          error => {
            console.log(error);
          }
        )
    
  }

  refrescar(): void {
    this.obtenerPersonas();
    this.seleccionada = {id: 0};
    this.indice = -1;
  }

  seleccionar(persona: Persona, indice: number): void {
    this.seleccionada = persona;
    this.indice = indice;
  }



}
