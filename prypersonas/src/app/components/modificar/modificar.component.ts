import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Persona } from 'src/app/models/persona.model';
import { SrvPersonasService } from 'src/app/services/srv-personas.service';


@Component({
  selector: 'app-modificar',
  templateUrl: './modificar.component.html',
  styleUrls: ['./modificar.component.css']
})
export class ModificarComponent implements OnInit {
  nuevo: Persona = {
    id: 0,
    nombre: '',
    fnacimiento:  new Date()
  
  };
  mensaje = '';
  lstPersonas?: Persona[ ];
 
  submitted = false;

   constructor(private servicio: SrvPersonasService, private route: ActivatedRoute) { }

  ngOnInit(): void {
     this.obtenerPersonas();
    this.obtener(this.route.snapshot.params.id);

  }

  obtener(id: number): void {
    try {
      
        this.servicio.obtener(id)
        .subscribe(
          datos =>{
            this.nuevo = datos;
              
          },
          error =>{
            console.log(error);
            this.mensaje = 'Se presentó un error.';  
          }
        )
  }
  catch{
    this.mensaje = 'Persona no registrada.';  
  }

  }

  actualizar(): void {
        const datos = {
          nombre: this.nuevo.nombre, 
          documento: this.nuevo.documento,
          fnacimiento: this.nuevo.fnacimiento,
          idmadre: this.nuevo.idmadre,
          idpadre: this.nuevo.idpadre
        };
        console.log(datos);
        try {
          this.servicio.actualizar(this.nuevo.id, datos)
              .subscribe(
                response => {console.log(response);
                this.submitted = true;},
                error => {console.log(error);
                  this.mensaje = 'Se presentó un error.';  
                }
                
              );
          }
          catch{
            this.mensaje = 'Se presentó un error al actualizar la persona.';  
          }

  }

  obtenerPersonas(): void {
    this.servicio.obtenerTodos()
        .subscribe(
          respuesta => {
            this.lstPersonas =respuesta;
           
          }, 
          error => {
            console.log(error);
          }
        )
        
  }


}
