import { Component, OnInit } from '@angular/core';
import { Persona } from 'src/app/models/persona.model';
import { SrvPersonasService } from 'src/app/services/srv-personas.service';


@Component({
  selector: 'app-adicionar',
  templateUrl: './adicionar.component.html',
  styleUrls: ['./adicionar.component.css']
})
export class AdicionarComponent implements OnInit {

  nuevo: Persona = {
    id: 0,
    nombre: '',
    fnacimiento:  new Date()
  };
  mensaje = '';
  lstPersonas?: Persona[ ];
  submitted = false;

   constructor(private servicio: SrvPersonasService) { }

  ngOnInit(): void {
    this.obtenerPersonas();
  }
  
  grabar(): void {
        const datos = {
          nombre: this.nuevo.nombre, 
          documento: this.nuevo.documento,
          fnacimiento: this.nuevo.fnacimiento,
          idmadre: this.nuevo.idmadre,
          idpadre: this.nuevo.idpadre
        };
        //console.log(datos);
        try{
            this.servicio.agregar(datos)
                .subscribe(
                  response => {
                   
                    this.submitted = true;
                    this.mensaje = 'La persona se adicionó satisfactoriamente.';
                    this.limpiar();},
                  error => {
                    console.log("Este es wel error");
                    console.log(error);
                    this.mensaje = 'Se presento un error al adicionar la persona. '+error.error;  
                  }
                  
                );
          }
          catch(error){
              this.mensaje = 'Se presento un error al adicionar la persona. ';  
          }

  }

  limpiar(): void{
    this.submitted = false;
    this.nuevo = {
      id : 0,
      nombre: '',
      fnacimiento:  new Date()
    }
  }


  obtenerPersonas(): void {
    this.servicio.obtenerTodos()
        .subscribe(
          respuesta => {
            this.lstPersonas =respuesta;
          }, 
          error => {
            console.log(error);
          }
        )
    
  }

}
