export class Persona {
    id?: number;
    nombre?: string;
    documento?: number;
    fnacimiento?: Date;
    idmadre?: number;
    idpadre?: number;
    HijoMadre?: Persona;
    HijoPadre?: Persona;

    constructor(){
        this.id = 0;
        this.nombre = '';
        this.documento = 0;
        this.fnacimiento =  new Date();
        this.HijoMadre= undefined;
        this.HijoPadre= undefined;

    }

}
