import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdicionarComponent } from './components/adicionar/adicionar.component';
import { ModificarComponent } from './components/modificar/modificar.component';
import { ListarComponent } from './components/listar/listar.component';

const routes: Routes = [
  {path:'', redirectTo:'listapersonas', pathMatch:'full'},
  {path:'listapersonas', component: ListarComponent},
  {path:'personas/:id', component: ModificarComponent },
  {path:'agregar', component: AdicionarComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
